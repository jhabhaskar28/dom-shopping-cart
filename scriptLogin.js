function validateForm(){

    let firstName = document.querySelector('input[class="firstName"]');
    let lastName = document.querySelector('input[class="lastName"]');
    let email = document.querySelector('input[class="email"]');
    let password = document.querySelector('input[class="password"]');
    let repeatPassword = document.querySelector('input[class="passwordRepeat"]');
    let checkBoxChecked = document.querySelector('input[type="checkbox"]').checked;
    let errorMessage = document.querySelector('.errorMessage');

    let firstNameError = document.querySelector('h4[class="firstName"]');
    let lastNameError = document.querySelector('h4[class="lastName"]');
    let emailError = document.querySelector('h4[class="email"]');
    let passwordError = document.querySelector('h5[class="password"]');
    let passwordErrorTwo = document.querySelector('h5[class="password2"]');
    let passwordRepeatError = document.querySelector('h4[class="passwordRepeat"]');

    let errorflag = 0;

    if(/[0-9]/.test(firstName.value) === true || /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(firstName.value) === true || firstName.value === " " || firstName.value === ''){
        errorflag = 1;
        firstNameError.style.display = "flex";
        firstNameError.textContent = "Please enter a valid First Name";
        firstName.style.border = "2px solid red";
    } else {
        firstNameError.style.display = "none";
        firstName.style.border = "1px solid lightGreen";
    }

    if(/[0-9]/.test(lastName.value) === true || /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(lastName.value) === true || lastName.value === " " || lastName.value === ''){
        errorflag = 1;
        lastNameError.style.display = "flex";
        lastNameError.textContent = "Please enter a valid Last Name";
        lastName.style.border = "2px solid red";
    } else {
        lastNameError.style.display = "none";
        lastName.style.border = "1px solid lightGreen";
    }

    if(email.value.includes('@') === false || email.value.indexOf('@') === email.value.length-1 || /[`!#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/.test(email.value) === true ||
        email.value.includes('.') === false ||  email.value.indexOf('.') === email.value.length-1){
        errorflag = 1;
        emailError.style.display = "flex";
        emailError.textContent = "Please enter a valid email address";
        email.style.border = "2px solid red";
    } else {
        emailError.style.display = "none";
        email.style.border = "2px solid lightGreen";
    }

    if (password.value.length < 8 || /[0-9]/.test(password.value) === false || /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(password.value) === false){
        errorflag = 1;
        passwordError.style.display = "flex";
        passwordErrorTwo.style.display = "flex";
        passwordError.textContent = 'Password should be atleast 8 characters long.';
        passwordErrorTwo.textContent = 'Atleast a number and a special character required.';
        password.style.border = "2px solid red";
    } else {
        passwordError.style.display = "none";
        passwordErrorTwo.style.display = "none";
        password.style.border = "2px solid lightGreen";
    }

    if(password.value !== repeatPassword.value){
        errorflag = 1;
        passwordRepeatError.style.display = "flex";
        passwordRepeatError.textContent = "Password and Repeat Password should not be different!";
        repeatPassword.style.border = "2px solid red";
    } else {
        passwordRepeatError.style.display = "none";
        repeatPassword.style.border = "2px solid lightGreen";
    }

    if (checkBoxChecked === false){
        errorflag = 1;
        errorMessage.style.display = "flex";
        errorMessage.textContent = "Please agree to the Terms Of Service!";
    } else {
        errorMessage.style.display = "none";
    }
    
    if(errorflag === 0){
        let form = document.querySelector('.signupForm');
        form.style.display = "none";
        let success = document.querySelector('.formSuccess');
        success.style.display = "flex";
    }

}

let submitButton = document.querySelector('#submitForm');
submitButton.addEventListener('click',validateForm);