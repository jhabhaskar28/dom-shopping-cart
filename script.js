fetch('https://fakestoreapi.com/products')
.then((response) => {
    return response.json();
})
.then((data) => {
    let loader = document.querySelector('.loaderLogo');
    loader.style.display = "none";

    if(data === undefined || data === null || (data.constructor === Array && data.length === 0)){

        let noProduct = document.createElement('h1');
        noProduct.textContent = "Sorry. No products to display!";

        let body = document.querySelector('body');
        let footer = document.querySelector('footer');

        body.insertBefore(noProduct,footer);
    } else {
        data.forEach((item) => {
            createCart(item);
        });
    }

    let header = document.querySelector('.header');
    header.style.display = "flex";
    
    let footer = document.querySelector('.footer');
    footer.style.display = "flex";
})
.catch((err) => {

    let loader = document.querySelector('.loaderLogo');
    loader.style.display = "none";

    let failedAPILoad = document.querySelector('.failedAPILoad');
    failedAPILoad.style.display = "flex";

    let body = document.querySelector('body');
    body.style.backgroundColor = "White";
    
    console.error(err);
});

function createCart(item){

    let itemDiv = document.createElement('div');
    itemDiv.className = "item";

    let imageContainer = document.createElement('div');
    imageContainer.className = "image";
    imageContainer.style.backgroundImage = `url(${item.image})`;

    let descriptionContainer = document.createElement('div');
    descriptionContainer.className = "description";

    let title = document.createElement('h3');
    title.textContent = item.title;

    let summary = document.createElement('p');
    summary.textContent = item.description;

    let category = document.createElement('h4');
    category.textContent = `Category: ${item.category}`;

    let rating = document.createElement('h4');
    rating.textContent = `Rating: ${item.rating.rate}`;

    let price = document.createElement('h3');
    price.textContent = `Price: $ ${item.price}`;

    let details = document.createElement('div');
    details.className = "details";

    details.appendChild(category);
    details.appendChild(rating);
    details.appendChild(price);

    descriptionContainer.appendChild(title);
    descriptionContainer.appendChild(summary);

    itemDiv.appendChild(imageContainer);
    itemDiv.appendChild(descriptionContainer);
    itemDiv.appendChild(details);

    let items = document.querySelector('.items');
    
    items.appendChild(itemDiv);
}